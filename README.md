# Secured-Preference-Store
Android的`SharedReferences`包装器，使用256位AES加密对内容进行加密。加密密钥安全地存储在设备的密钥库中。您还可以使用`EncryptionManager`类对开箱即用的数据进行加密和解密。

## Setup
### Maven
```
<dependency>
  <groupId>online.devliving</groupId>
  <artifactId>securedpreferencestore</artifactId>
  <version>latest_version</version>
  <type>pom</type>
</dependency>
````

### Gradle
```
compile 'online.devliving:securedpreferencestore:latest_version'
```

## Usage
在首次使用存储之前，必须对其进行初始化
```java
//not mandatory, can be null too
String storeFileName = "securedStore";
//not mandatory, can be null too
String keyPrefix = "vss";
//it's better to provide one, and you need to provide the same key each time after the first time
byte[] seedKey = "SecuredSeedData".getBytes();
SecuredPreferenceStore.init(getApplicationContext(), storeFileName, keyPrefix, seedKey, new DefaultRecoveryHandler());
```
可以在 `Application` 或者 `Activity`的 `onCreate`中


您可以像使用默认值一样使用受保护的首选项存储 `SharedPreferences`
```java
SecuredPreferenceStore prefStore = SecuredPreferenceStore.getSharedInstance();
prefStore.putString(TEXT_1, text1.length() > 0 ? text1.getText() : null).flush();
prefStore.putString(TEXT_2, text2.length() > 0 ? text2.getText() : null).flushSync();
prefStore.putInt(NUMBER_1, number1.length() > 0 ? Integer.parseInt(number1.getText().trim()) : 0).flushSync();
prefStore.putFloat(NUMBER_2, number2.length() > 0 ? Float.parseFloat(number2.getText().trim()) : 0).flushSync();
prefStore.putString(DATE_1, date1.length() > 0 ? date1.getText() : null).flushSync();
```
您可以访问基础加密管理器来加密/解密数据：
```java
EncryptionManager encryptionManager = SecuredPreferenceStore.getSharedInstance().getEncryptionManager();
```

您也可以使用独立的 `EncryptionManager` 自行加密/解密数据：
```java
SharedPreferences preferences = getSharedPreferences("backingStore", MODE_PRIVATE);
//not mandatory, can be null too
String keyAliasPrefix = "kp";
//not mandatory, can be null too
byte[] bitShiftKey = "bitShiftBits".getBytes();
EncryptionManager encryptionManager = new EncryptionManager(getApplicationContext(), preferences,
    keyAliasPrefix, bitShiftKey, new SecuredPreferenceStore.KeyStoreRecoveryNotifier() {
	@Override
	public boolean onRecoveryRequired(Exception e, KeyStore keyStore, List<String> keyAliases) {
	    return false;
	}
});
EncryptionManager.EncryptedData encryptedData = encryptionManager.encrypt(bytesToEncrypt);
byte[] decryptedData = encryptionManager.decrypt(encryptedData);
```
## Sample file content
示例加密件如下所示：

```xml
<?xml version='1.0' encoding='utf-8' standalone='yes' ?>
<map>
    <string name="11CD15241CB4D6F953FA27C76F72C10920C5FADF14FF2824104FA5D67D25B43C">ZMnr87IlDKg81hKw2SQ6Lw==]dhP/ymX7CMSaCkP6jQvNig==</string>
    <string name="C8D076EFD8542A5F02F86B176F667B42BFB9B1472E974E6AF31EB27CEA5689D4">JQ6Y4TQ/Y3iYw7KtatkqAg==]P+gpavV0MXiy1Qg0UHlBMg==</string>
    <string name="F2AA713F406544A3E9ABA20A32364FA29613F01C867B3D922A85DF4FA54FA13D">jMH1Wjnk0vehHOogT27HRA==]e8UHX1ihYjtP6Cv8dWdHLBptLwowt6IojKYa+1jkeH4=</string>
    <string name="C06C6027E72B7CE947885F6ADE3A73E338881197DBE02D8B7B7248F629BE26DA">EAGwO8u2ZPdxwdpAwPlu6A==]797VOGtpzDBO1ZU3m+Sb1A==</string>
    <string name="33188AFFEC74B412765C3C86859DE4620B5427C774D92F9026D95A7A8AAE1F96">s0b5h8XNnerci5AtallCQziSbqpm+ndjIsAQQadSxM+xzw7865sE3P+hbxGmMAQQj0kK35/C//eA
MXuQ0N/F+oapBiDIKdRt2GJB3wJ+eshuh6TcEv+J8NQhqn1eO2fdao353XthHpRomIeGEWLvB4Yd
7G5YYIajLWOGWzQVsMTg1eqdcJ7+BAMXdOdWhjTTo91NvhvykgLMC03FsePOZ/X8ej4vByH1i0en
hJCiChk90AQ9FhSkaF/Oum9KoWqg7NU0PGurK755VZflXfyn1vZ8hhTulW7BrA2o9HvT9tbju+bk
4yJ5lMxgS6o4b+0tqo+H4TPOUiZPgehTwsrzJg==
    </string>
    <string name="9DCB904DFDA83286B41A329A7D8648B0BFF73C63E844C88800B2AA5119204845">XPuUd1t97pnwsOzzHY3OCA==]xqXJrEfcgDhYo2K4TTAvY9IQwP/tGctd4Fa1JT/1sB8=</string>
</map>
```

## NOTICE

存储在 `KeyStore `中的密钥在静止时没有加密以避免[问题](https://code.google.com/p/android/issues/detail?id=61989)当设备的锁屏保护更改时，它们会被删除。因此，如果设备没有硬件支持的密钥存储，则密钥可能处于易受攻击的状态。你可以在[这里了解更多](http://doridori.github.io/android-security-the-forgetful-keystore).

## Recovery

当用户更改设备的安全性（屏幕锁定保护）时，密钥在低于21**的API级别中被**删除/锁定，有时在某些设备上的更高版本的API上被删除/锁定。这种现象是由于`Keystore`实现中的一些问题造成的，即[61989](https://code.google.com/p/android/issues/detail?id=61989), [177459](https://code.google.com/p/android/issues/detail?id=177459). 在这些问题得到解决之前，我们需要一种方法从该场景中恢复，否则应用程序本身可能无法使用。要启用恢复，您可以在首次调用`getSharedInstance`之前将`RecoveryHandler`添加到`securedReferenceStore`。

```java
SecuredPreferenceStore.setRecoveryHandler(new RecoveryHandler() {
            @Override
            protected void recover(Exception e, KeyStore keyStore, List<String> keyAliases, SharedPreferences preferences) {
                //Your recovery code goes here
            }
        });
```
库中包含一个名为` DefaultRecoveryHandler` 的默认恢复处理程序，该处理程序删除密钥和数据，使库有机会重新开始。

## License

	Licensed under the Apache License, Version 2.0 (the "License");
	you may not use this file except in compliance with the License.
	You may obtain a copy of the License at
	
	   http://www.apache.org/licenses/LICENSE-2.0
	
	Unless required by applicable law or agreed to in writing, software
	distributed under the License is distributed on an "AS IS" BASIS,
	WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
	See the License for the specific language governing permissions and
	limitations under the License.
	
	Copyright 2017 Mehedi Hasan Khan