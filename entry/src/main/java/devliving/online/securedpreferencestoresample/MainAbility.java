package devliving.online.securedpreferencestoresample;

import devliving.online.securedpreferencestoresample.slice.MainAbilitySlice;
import devliving.online.securedpreferencestoresample.utils.CommonData;
import ohos.aafwk.ability.Ability;
import ohos.aafwk.content.Intent;

public class MainAbility extends Ability {
    @Override
    public void onStart(Intent intent) {
        super.onStart(intent);
        super.setMainRoute(MainAbilitySlice.class.getName());
        addActionRoute(CommonData.FILEDEMO_PAGE, FileDemoAbility.class.getName());
    }

}
