package devliving.online.securedpreferencestoresample;

import devliving.online.securedpreferencestore.Logger;
import devliving.online.securedpreferencestoresample.slice.FileDemoAbilitySlice;
import devliving.online.securedpreferencestoresample.utils.CommonData;
import ohos.aafwk.ability.Ability;
import ohos.aafwk.content.Intent;
import ohos.aafwk.content.Operation;
import ohos.bundle.IBundleManager;
import ohos.event.commonevent.CommonEventData;
import ohos.event.commonevent.CommonEventManager;
import ohos.rpc.RemoteException;

public class FileDemoAbility extends Ability {

    private final int MY_PERMISSIONS_REQUEST_READ_USER_STORAGE = 102;

    @Override
    public void onStart(Intent intent) {
        super.onStart(intent);
        super.setMainRoute(FileDemoAbilitySlice.class.getName());
    }

    @Override
    public void onRequestPermissionsFromUserResult (int requestCode, String[] permissions, int[] grantResults) {
        switch (requestCode) {
            case MY_PERMISSIONS_REQUEST_READ_USER_STORAGE: {
                // 匹配requestPermissions的requestCode
                if (grantResults.length > 0
                        && grantResults[0] == IBundleManager.PERMISSION_GRANTED) {
                    Logger.e("-->>  同意");
                    sendEvent(true);
                    // 权限被授予
                    // 注意：因时间差导致接口权限检查时有无权限，所以对那些因无权限而抛异常的接口进行异常捕获处理
                } else {
                    // 权限被拒绝
                    sendEvent(false);
                }
                return;
            }
        }
    }

    private void sendEvent(boolean isGetPermission) {
        Logger.i("sendEvent......");
        try {
            Intent intent = new Intent();
            Operation operation = new Intent.OperationBuilder()
                    .withAction(CommonData.GET_PERMISSION)
                    .build();
            intent.setOperation(operation);
            intent.setParam(CommonData.IS_GET_PERMISSION, isGetPermission);
            CommonEventData eventData = new CommonEventData(intent);
            CommonEventManager.publishCommonEvent(eventData);
        } catch (RemoteException e) {
            Logger.e("publishCommonEvent occur exception.");
        }
    }

}
