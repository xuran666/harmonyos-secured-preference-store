package devliving.online.securedpreferencestoresample.utils;

import devliving.online.securedpreferencestoresample.FileDemoAbility;
import devliving.online.securedpreferencestoresample.MainAbility;

/**
 * CommonData Util
 *
 * @since 2021-01-11
 */
public interface CommonData {
    String ABILITY_MAIN = MainAbility.class.getName();

    String ABILITY_FILEDEMO = FileDemoAbility.class.getName();

    String  FILEDEMO_PAGE ="action.system.filedemo";

    String  GET_PERMISSION ="get_permission";

    String  IS_GET_PERMISSION ="is_get_permission";
}
