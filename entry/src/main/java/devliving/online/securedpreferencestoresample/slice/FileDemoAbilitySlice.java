package devliving.online.securedpreferencestoresample.slice;

import devliving.online.securedpreferencestore.Logger;
import devliving.online.securedpreferencestore.SecuredPreferenceStore;
import devliving.online.securedpreferencestoresample.ResourceTable;
import devliving.online.securedpreferencestoresample.utils.CommonData;
import ohos.aafwk.ability.AbilitySlice;
import ohos.aafwk.ability.DataAbilityHelper;
import ohos.aafwk.ability.DataAbilityRemoteException;
import ohos.aafwk.content.Intent;
import ohos.agp.components.*;
import ohos.bundle.IBundleManager;
import ohos.event.commonevent.*;
import ohos.rpc.RemoteException;
import ohos.utils.net.Uri;

import java.io.*;

public class FileDemoAbilitySlice extends AbilitySlice implements Component.ClickedListener {

    Switch modeToggle;
    Text outputFileText;
    Text inputFileText;
    Button chooseImageBtn, processBtn;

    Uri inputUri, outputUri;

    private MyCommonEventSubscriber subscriber;

    private final int FILE_SELECT_CODE = 101;
    private final int MY_PERMISSIONS_REQUEST_READ_USER_STORAGE = 102;

    @Override
    public void onStart(Intent intent) {
        super.onStart(intent);
        super.setUIContent(ResourceTable.Layout_ability_file_demo);
        initView();
        updateProcessButton();
        subscribe();
    }

    void initView() {
        modeToggle = (Switch) findComponentById(ResourceTable.Id_modeToggle);
        inputFileText = (Text) findComponentById(ResourceTable.Id_inputFile);
        outputFileText = (Text) findComponentById(ResourceTable.Id_outputFile);
        chooseImageBtn = (Button) findComponentById(ResourceTable.Id_loadImage);
        processBtn = (Button) findComponentById(ResourceTable.Id_processImage);
        chooseImageBtn.setClickedListener(this);
        processBtn.setClickedListener(this);
        modeToggle.setCheckedStateChangedListener((view, isChecked) -> updateProcessButton());
    }

    void updateProcessButton() {
        String text = modeToggle.isChecked() ? "Encrypt" : "Decrypt";
        processBtn.setText(text);
    }

    private void openFileChooser() {
        Intent intent = new Intent();
        intent.setAction("android.intent.action.GET_CONTENT");
        intent.setType("*/*");
        intent.addEntity("android.intent.category.OPENABLE");
        intent.setParam("android.intent.extra.LOCAL_ONLY", true);
        startAbilityForResult(intent, FILE_SELECT_CODE);
    }

    @Override
    protected void onAbilityResult(int requestCode, int resultCode, Intent resultData) {
        if (requestCode == FILE_SELECT_CODE && resultCode == -1) {
            inputUri = resultData.getUri();
            inputFileText.setText(inputUri.getDecodedPath());
        }
    }

//    String getFileExtension(Uri uri) {
//        ContentResolver contentResolver = getContentResolver();
//        String extension;
//        if(uri.getScheme() == ContentResolver.SCHEME_CONTENT) {
//            extension = MimeTypeMap.getSingleton().getExtensionFromMimeType(contentResolver.getType(uri));
//        } else {
//            extension = MimeTypeMap.getFileExtensionFromUrl(Uri.getUriFromFile(new File(uri.getPath())).toString());
//        }
//
//        return extension;
//    }

//    String findFileName(Uri uri) {
//        String displayName = null;
//        Cursor cursor = getContentResolver()
//                .query(uri, null, null, null, null, null);
//
//        try {
//            // moveToFirst() returns false if the cursor has 0 rows.  Very handy for
//            // "if there's anything to look at, look at it" conditionals.
//            if (cursor != null && cursor.moveToFirst()) {
//
//                // Note it's called "Display Name".  This is
//                // provider-specific, and might not necessarily be the file name.
//                displayName = cursor.getString(
//                        cursor.getColumnIndex(OpenableColumns.DISPLAY_NAME));
//                Logger.i("Display Name: " + displayName);
//            }
//        } finally {
//            if(cursor != null) cursor.close();
//        }
//
//        return displayName;
//    }

    class MyCommonEventSubscriber extends CommonEventSubscriber {
        MyCommonEventSubscriber(CommonEventSubscribeInfo info) {
            super(info);
        }

        @Override
        public void onReceiveEvent(CommonEventData commonEventData) {
            Intent intent = commonEventData.getIntent();
            boolean isGetPersmission = intent.getBooleanParam(CommonData.IS_GET_PERMISSION, false);
            if (isGetPersmission) {
                processFile();
            } else {
                Logger.i("没给权限");
            }
            Logger.i("onReceiveEvent.....");
        }
    }

    private void subscribe() {
        MatchingSkills matchingSkills = new MatchingSkills();
        matchingSkills.addEvent(CommonData.GET_PERMISSION);
        matchingSkills.addEvent(CommonEventSupport.COMMON_EVENT_SCREEN_ON);
        CommonEventSubscribeInfo subscribeInfo = new CommonEventSubscribeInfo(matchingSkills);
        subscriber = new MyCommonEventSubscriber(subscribeInfo);
        try {
            CommonEventManager.subscribeCommonEvent(subscriber);
        } catch (RemoteException e) {
            Logger.e("subscribeCommonEvent occur exception.");
        }
    }

    private void unSubscribe() {
        try {
            CommonEventManager.unsubscribeCommonEvent(subscriber);
        } catch (RemoteException e) {
            Logger.e("unSubscribe Exception");
        }
    }

    public void requestPermission() {
        if (verifySelfPermission("ohos.permission.READ_USER_STORAGE") != IBundleManager.PERMISSION_GRANTED) {
            // 应用未被授予权限
            if (canRequestPermission("ohos.permission.READ_USER_STORAGE")) {
                // 是否可以申请弹框授权(首次申请或者用户未选择禁止且不再提示)
                requestPermissionsFromUser(
                        new String[]{"ohos.permission.READ_USER_STORAGE"}, MY_PERMISSIONS_REQUEST_READ_USER_STORAGE);
            } else {
                // 显示应用需要权限的理由，提示用户进入设置授权
            }
        } else {
            Logger.e("---->  给过权限，进行下一步");
            // 权限已被授予
            processFile();
        }
    }

    private void processFile() {
//        DataAbilityHelper helper = DataAbilityHelper.creator(this);
//        FileDescriptor fd = null;
//        try {
//            fd = helper.openFile(inputUri, "rw");
//        } catch (DataAbilityRemoteException e) {
//            e.printStackTrace();
//        } catch (FileNotFoundException e) {
//            e.printStackTrace();
//        }
//        FileInputStream fis = new FileInputStream(fd);
//        if(inputUri != null) {
//            processBtn.setEnabled(false);
//
//            String fileName = findFileName(inputUri);
//            if(fileName == null) {
//        new ToastDialog(getContext())
//                .setText("Error: Filename could not be found")
//                .show();
//                fileName = UUID.randomUUID().toString();
//                String extension = getFileExtension(inputUri);
//
//                if(extension == null) {
//        new ToastDialog(getContext())
//                .setText("Error: File extension could not be found")
//                .show();
//                    return;
//                } else {
//                    fileName = fileName + "." + extension;
//                }
//            }
////
//            String prefix = modeToggle.isChecked() ? "ENC" : "DEC";
//            fileName = prefix + fileName;
//
        try {
            File outputFile = new File(getExternalFilesDir(null), System.currentTimeMillis() + "");
            BufferedInputStream fileIn = new BufferedInputStream(new FileInputStream(inputUri.getEncodedPath()));
            BufferedOutputStream fileOut = new BufferedOutputStream(new FileOutputStream(outputFile));

            if (modeToggle.isChecked()) {
                SecuredPreferenceStore.getSharedInstance().getEncryptionManager().tryEncrypt(fileIn, fileOut);
            } else {
                SecuredPreferenceStore.getSharedInstance().getEncryptionManager().tryDecrypt(fileIn, fileOut);
            }

            outputUri = Uri.getUriFromFile(outputFile);
            outputFileText.setText(outputUri.getDecodedPath());
            fileIn.close();
            fileOut.close();
        } catch (Exception e) {
            Logger.e(e);
        }
//        }
    }

    @Override
    public void onActive() {
        super.onActive();
    }

    @Override
    public void onForeground(Intent intent) {
        super.onForeground(intent);
    }

    @Override
    public void onClick(Component component) {
        int btnId = component.getId();
        switch (btnId) {
            case ResourceTable.Id_loadImage:
                openFileChooser();
                break;
            case ResourceTable.Id_processImage:
                requestPermission();
//                processFile();
                break;
        }
    }
}
