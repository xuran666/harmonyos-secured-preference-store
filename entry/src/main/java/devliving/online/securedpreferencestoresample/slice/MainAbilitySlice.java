package devliving.online.securedpreferencestoresample.slice;

import devliving.online.securedpreferencestore.DefaultRecoveryHandler;
import devliving.online.securedpreferencestore.Logger;
import devliving.online.securedpreferencestore.SecuredPreferenceStore;
import devliving.online.securedpreferencestoresample.ResourceTable;
import devliving.online.securedpreferencestoresample.utils.CommonData;
import ohos.aafwk.ability.AbilitySlice;
import ohos.aafwk.content.Intent;
import ohos.aafwk.content.Operation;
import ohos.agp.components.Button;
import ohos.agp.components.Component;
import ohos.agp.components.TextField;
import ohos.agp.window.dialog.ToastDialog;
import ohos.data.DatabaseHelper;
import ohos.data.preferences.Preferences;
import ohos.hiviewdfx.HiLog;
import ohos.hiviewdfx.HiLogLabel;

import java.security.KeyStore;
import java.util.List;

public class MainAbilitySlice extends AbilitySlice {

    static final HiLogLabel LABEL = new HiLogLabel(HiLog.LOG_APP, 0x00201, "SECURED-PREFERENCE");
    TextField text1, number1, date1, text2, number2;
    Button reloadButton, saveButton, imageDemoBtn;
    final String TEXT_1 = "text_short", TEXT_2 = "text_long", NUMBER_1 = "number_int", NUMBER_2 = "number_float", DATE_1 = "date_text", DATE_2 = "date_long";

    @Override
    public void onStart(Intent intent) {
        super.onStart(intent);
        super.setUIContent(ResourceTable.Layout_ability_main);

        text1 = (TextField) findComponentById(ResourceTable.Id_text_value_1);
        number1 = (TextField) findComponentById(ResourceTable.Id_number_1);
        date1 = (TextField) findComponentById(ResourceTable.Id_date_1);

        text2 = (TextField) findComponentById(ResourceTable.Id_text_value_2);
        number2 = (TextField) findComponentById(ResourceTable.Id_number_2);

        reloadButton = (Button) findComponentById(ResourceTable.Id_reload);
        saveButton = (Button) findComponentById(ResourceTable.Id_save);
        imageDemoBtn = (Button) findComponentById(ResourceTable.Id_tryFile);

        initPre();

        reloadButton.setClickedListener(new Component.ClickedListener() {
            @Override
            public void onClick(Component component) {
                try {
                    reloadData();
                } catch (Exception e) {
                    HiLog.error(LABEL, e.toString());
                    new ToastDialog(getContext())
                            .setText("An exception occurred, see log for details")
                            .show();
                }
            }
        });

        saveButton.setClickedListener(new Component.ClickedListener() {
            @Override
            public void onClick(Component component) {
                try {
                    saveData();
                } catch (Exception e) {
                    HiLog.error(LABEL, e.toString());
                    new ToastDialog(getContext())
                            .setText("An exception occurred, see log for details")
                            .show();
                }
            }
        });

        imageDemoBtn.setClickedListener(new Component.ClickedListener() {
            @Override
            public void onClick(Component component) {
                Intent mathGameIntent = new Intent();
                Operation operationMath = new Intent.OperationBuilder()
                        .withBundleName(getBundleName())
                        .withAbilityName(CommonData.ABILITY_FILEDEMO)
                        .withAction(CommonData.FILEDEMO_PAGE)
                        .build();
                mathGameIntent.setOperation(operationMath);
                startAbility(mathGameIntent);
            }
        });

    }

    private void initPre(){
        try {
            //not mandatory, can be null too
            String storeFileName = "securedStore";
            //not mandatory, can be null too
            String keyPrefix = "vss";
            //it's better to provide one, and you need to provide the same key each time after the first time
            byte[] seedKey = "seed".getBytes();
            SecuredPreferenceStore.init(getApplicationContext(), storeFileName, keyPrefix, seedKey, new DefaultRecoveryHandler());
            Logger.i("init success");
            //SecuredPreferenceStore.init(getApplicationContext(), null);
            setupStore();
        } catch (Exception e) {
            // Handle error.
            Logger.e(e);
        }
    }

    private void setupStore() {
        SecuredPreferenceStore.setRecoveryHandler(new DefaultRecoveryHandler() {
            @Override
            protected boolean recover(Exception e, KeyStore keyStore, List<String> keyAliases, Preferences preferences) {
                new ToastDialog(getContext())
                        .setText("Encryption key got invalidated, will try to start over.")
                        .show();
                return super.recover(e, keyStore, keyAliases, preferences);
            }
        });

        try {
            reloadData();
        } catch (Exception e) {
            Logger.e(e);
            new ToastDialog(getContext())
                    .setText("An exception occurred, see log for details")
                    .show();
        }
    }

    void reloadData() {
        SecuredPreferenceStore prefStore = SecuredPreferenceStore.getSharedInstance();

        String textShort = prefStore.getString(TEXT_1, null);
        String textLong = prefStore.getString(TEXT_2, null);
        int numberInt = prefStore.getInt(NUMBER_1, 0);
        float numberFloat = prefStore.getFloat(NUMBER_2, 0);
        String dateText = prefStore.getString(DATE_1, null);

        text1.setText(textShort);
        text2.setText(textLong);
        number1.setText(String.valueOf(numberInt));
        number2.setText(String.valueOf(numberFloat));
        date1.setText(dateText);
    }

    void saveData() {
        SecuredPreferenceStore prefStore = SecuredPreferenceStore.getSharedInstance();
        prefStore.putString(TEXT_1, text1.length() > 0 ? text1.getText() : null).flush();
        prefStore.putString(TEXT_2, text2.length() > 0 ? text2.getText() : null).flushSync();

        prefStore.putInt(NUMBER_1, number1.length() > 0 ? Integer.parseInt(number1.getText().trim()) : 0).flushSync();
        prefStore.putFloat(NUMBER_2, number2.length() > 0 ? Float.parseFloat(number2.getText().trim()) : 0).flushSync();

        prefStore.putString(DATE_1, date1.length() > 0 ? date1.getText() : null).flushSync();
    }

    @Override
    public void onActive() {
        super.onActive();
    }

    @Override
    public void onForeground(Intent intent) {
        super.onForeground(intent);
    }
}
