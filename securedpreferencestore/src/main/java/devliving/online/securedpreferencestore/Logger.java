package devliving.online.securedpreferencestore;

import ohos.agp.render.render3d.BuildConfig;
import ohos.hiviewdfx.HiLog;
import ohos.hiviewdfx.HiLogLabel;

public class Logger {
    private static boolean enabled = BuildConfig.DEBUG;
    //    private static final String TAG = "SECURED-PREFERENCE";
    static final HiLogLabel LABEL = new HiLogLabel(HiLog.LOG_APP, 0x0, "SECURED-PREFERENCE");

    public static void forceLoggingOn() {
        enabled = true;
    }

    public static void i(String message) {
        if (enabled) {
            HiLog.info(LABEL, message);
        }
    }

    public static void d(String message) {
        if (enabled) {
            HiLog.debug(LABEL, message);
        }
    }

    public static void w(String message) {
        if (enabled) {
            HiLog.warn(LABEL, message);
        }
    }

    public static void e(String message) {
        if (enabled) {
            HiLog.error(LABEL, message);
        }
    }

    public static void d(Exception e) {
        if (enabled) {
            HiLog.debug(LABEL, "Exception was thrown", e);
        }
    }

    public static void w(Exception e) {
        if (enabled) {
            HiLog.warn(LABEL, "Exception was thrown", e);
        }
    }

    public static void e(Exception e) {
        if (enabled) {
            HiLog.error(LABEL, "Exception was thrown", e);
        }
    }

    public static void d(String message, Exception e) {
        if (enabled) {
            HiLog.debug(LABEL, message, e);
        }
    }

    public static void w(String tag, String message, Exception e) {
        if (enabled) {
            HiLog.warn(LABEL, message, e);
        }
    }

    public static void e(String message, Exception e) {
        if (enabled) {
            HiLog.error(LABEL, message, e);
        }
    }
}
