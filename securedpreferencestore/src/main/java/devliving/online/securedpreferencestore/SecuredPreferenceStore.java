package devliving.online.securedpreferencestore;

import ohos.agp.render.render3d.BuildConfig;
import ohos.app.Context;
import ohos.data.DatabaseHelper;
import ohos.data.preferences.Preferences;

import javax.crypto.BadPaddingException;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.security.*;
import java.security.cert.CertificateException;
import java.util.*;

/**
 * Created by Mehedi on 8/21/16.
 */
public final class SecuredPreferenceStore implements Preferences {
    private static final int[] VERSIONS_WITH_BREAKING_CHANGES = new int[]{10}; //version code in ascending order
    static final String VERSION_KEY = "VERSION";
    private static final String DEFAULT_PREF_FILE_NAME = "SPS_file";

    private final String[] RESERVED_KEYS;

    private Preferences mPrefs;
    private EncryptionManager mEncryptionManager;

    private static RecoveryHandler mRecoveryHandler;

    private static SecuredPreferenceStore mInstance;
    private final DatabaseHelper mDatabaseHelper;
    private final int DEFAULT_NUM = 9;
    private final int DEFAULT_NUM_TEN = 10;

    /**
     * @param appContext     application context
     * @param storeName      optional name of the preference file
     * @param keyPrefix      optional prefix for encryption key aliases
     * @param bitShiftingKey seed for randomization and bit shifting, enhances security on older OS versions
     * @throws IOException
     * @throws CertificateException
     * @throws NoSuchAlgorithmException
     * @throws KeyStoreException
     * @throws UnrecoverableEntryException
     * @throws InvalidAlgorithmParameterException
     * @throws NoSuchPaddingException
     * @throws InvalidKeyException
     * @throws NoSuchProviderException
     * @throws MigrationFailedException
     */
    private SecuredPreferenceStore(Context appContext, String storeName, String keyPrefix,
                                   byte[] bitShiftingKey) throws IOException, CertificateException, NoSuchAlgorithmException, KeyStoreException, UnrecoverableEntryException, InvalidAlgorithmParameterException, NoSuchPaddingException, InvalidKeyException, NoSuchProviderException, MigrationFailedException {
        Logger.d("Creating store instance");
        // handle migration
        String fileName = storeName != null ? storeName : DEFAULT_PREF_FILE_NAME;
        mDatabaseHelper = new DatabaseHelper(appContext);
        mPrefs = mDatabaseHelper.getPreferences(fileName);

        int mRunningVersion = mPrefs.getInt(VERSION_KEY, DEFAULT_NUM);

        if (mRunningVersion < BuildConfig.VERSION_CODE) {
            new MigrationHelper(appContext, storeName, keyPrefix, bitShiftingKey)
                    .migrate(mRunningVersion, BuildConfig.VERSION_CODE);
        }

        mEncryptionManager = new EncryptionManager(appContext, mPrefs, keyPrefix, bitShiftingKey, new KeyStoreRecoveryNotifier() {
            @Override
            public boolean onRecoveryRequired(Exception e, KeyStore keyStore, List<String> keyAliases) {
                if (mRecoveryHandler != null) {
                    return mRecoveryHandler.recover(e, keyStore, keyAliases, mPrefs);
                } else {
                    throw new RuntimeException(e);
                }
            }
        });

        RESERVED_KEYS = new String[]{VERSION_KEY, EncryptionManager.OVERRIDING_KEY_ALIAS_PREFIX_NAME,
                mEncryptionManager.IS_COMPAT_MODE_KEY_ALIAS, mEncryptionManager.MAC_KEY_ALIAS,
                mEncryptionManager.AES_KEY_ALIAS};
    }

    public static void setRecoveryHandler(RecoveryHandler recoveryHandler) {
        SecuredPreferenceStore.mRecoveryHandler = recoveryHandler;
    }

    public static synchronized SecuredPreferenceStore getSharedInstance() {
        if (mInstance == null) {
            throw new IllegalStateException("Must call init() before using the store");
        }

        return mInstance;
    }

    /**
     * Must be called once before using the SecuredPreferenceStore to initialize the shared instance.
     * You may call it in @code{onCreate} method of your application class or launcher activity
     *
     * @param appContext      application context
     * @param storeName       optional name of the preference file
     * @param keyPrefix       optional prefix for encryption key aliases
     * @param bitShiftingKey  seed for randomization and bit shifting, enhances security on older OS versions
     * @param recoveryHandler recovery handler to use if necessary
     * @throws IOException
     * @throws CertificateException
     * @throws NoSuchAlgorithmException
     * @throws KeyStoreException
     * @throws UnrecoverableEntryException
     * @throws InvalidAlgorithmParameterException
     * @throws NoSuchPaddingException
     * @throws InvalidKeyException
     * @throws NoSuchProviderException
     */
    public static void init(Context appContext, String storeName, String keyPrefix, byte[] bitShiftingKey,
                            RecoveryHandler recoveryHandler) throws IOException, CertificateException, NoSuchAlgorithmException, KeyStoreException, UnrecoverableEntryException, InvalidAlgorithmParameterException, NoSuchPaddingException, InvalidKeyException, NoSuchProviderException, MigrationFailedException {

        if (mInstance != null) {
            Logger.w("init called when there already is a non-null instance of the class");
            return;
        }

        setRecoveryHandler(recoveryHandler);
        mInstance = new SecuredPreferenceStore(appContext, storeName, keyPrefix, bitShiftingKey);
    }

    /**
     * @see #init(Context, String, String, byte[], RecoveryHandler)
     * @deprecated Use the full constructor for better security, specially on older OS versions
     */
    public static void init(Context appContext, RecoveryHandler recoveryHandler) throws IOException, CertificateException, NoSuchAlgorithmException, InvalidKeyException, UnrecoverableEntryException, InvalidAlgorithmParameterException, NoSuchPaddingException, NoSuchProviderException, KeyStoreException, MigrationFailedException {
        init(appContext, DEFAULT_PREF_FILE_NAME, null, null, recoveryHandler);
    }

    public EncryptionManager getEncryptionManager() {
        return mEncryptionManager;
    }

    private boolean isReservedKey(String key) {
        return Arrays.asList(RESERVED_KEYS).contains(key);
    }

    private boolean isReservedHashedKey(String hashedKey) {
        for (String key : RESERVED_KEYS) {
            try {
                if (hashedKey.equals(EncryptionManager.getHashed(key))) {
                    return true;
                }
            } catch (NoSuchAlgorithmException e) {
                Logger.e(e);
            } catch (UnsupportedEncodingException e) {
                Logger.e(e);
            }
        }

        return false;
    }

    @Override
    public Map<String, Object> getAll() {
        Map<String, ?> all = mPrefs.getAll();
        Map<String, Object> dAll = new HashMap<>(all.size());

        if (all.size() > 0) {
            for (String key : all.keySet()) {
                if (key.equals(VERSION_KEY) || isReservedHashedKey(key)) {
                    continue;
                }
                try {
                    Object value = all.get(key);
                    dAll.put(key, mEncryptionManager.decrypt((String) value));
                } catch (Exception e) {
                    Logger.e(e);
                }
            }
        }
        return dAll;
    }

    @Override
    public String getString(String key, String defValue) {
        if (!isReservedKey(key)) {
            try {
                String hashedKey = EncryptionManager.getHashed(key);
                String value = mPrefs.getString(hashedKey, null);
                if (value != null) {
                    return mEncryptionManager.decrypt(value);
                }
            } catch (Exception e) {
                Logger.e(e);
            }
        }

        return defValue;
    }

    @Override
    public Set<String> getStringSet(String key, Set<String> defValues) {
        if (!isReservedKey(key)) {
            try {
                String hashedKey = EncryptionManager.getHashed(key);
                Set<String> eSet = mPrefs.getStringSet(hashedKey, null);

                if (eSet != null) {
                    Set<String> dSet = new HashSet<>(eSet.size());

                    for (String val : eSet) {
                        dSet.add(mEncryptionManager.decrypt(val));
                    }

                    return dSet;
                }

            } catch (Exception e) {
                Logger.e(e);
            }
        }

        return defValues;
    }

    @Override
    public int getInt(String key, int defValue) {
        String value = getString(key, null);
        if (value != null) {
            return Integer.parseInt(value);
        }
        return defValue;
    }

    @Override
    public long getLong(String key, long defValue) {
        String value = getString(key, null);
        if (value != null) {
            return Long.parseLong(value);
        }
        return defValue;
    }

    @Override
    public float getFloat(String key, float defValue) {
        String value = getString(key, null);
        if (value != null) {
            return Float.parseFloat(value);
        }
        return defValue;
    }

    @Override
    public boolean getBoolean(String key, boolean defValue) {
        String value = getString(key, null);
        if (value != null) {
            return Boolean.parseBoolean(value);
        }
        return defValue;
    }

    public byte[] getBytes(String key) {
        String val = getString(key, null);
        if (val != null) {
            return EncryptionManager.base64Decode(val);
        }

        return null;
    }

    public interface KeyStoreRecoveryNotifier {
        /**
         * @param e
         * @param keyStore
         * @param keyAliases
         * @return true if the error could be resolved
         */
        boolean onRecoveryRequired(Exception e, KeyStore keyStore, List<String> keyAliases);
    }

    //region Migration
    private class MigrationHelper {
        String storeName, keyPrefix;
        byte[] bitShiftKey;
        Context mContext;

        public MigrationHelper(Context context, String storeName, String keyPrefix, byte[] bitShiftKey) {
            this.storeName = storeName;
            this.keyPrefix = keyPrefix;
            this.bitShiftKey = bitShiftKey;
            mContext = context;
        }

        /**
         * if storeName has changed from the default and there's data in the default file then those will be moved to the new file
         * if keyPrefix has changed from the default and there aren't any other prefix stored in the file, then new keys will be stored
         * with the new prefix and existing data will be migrated
         *
         * @throws MigrationFailedException
         * @throws
         */
        void migrateToV10() throws MigrationFailedException {
            if (storeName == null && keyPrefix == null && bitShiftKey == null) {
                //using the defaults, so no migration needed
                return;
            }

            Preferences prefToRead, prefToWrite;

            prefToRead = prefToWrite = mDatabaseHelper.getPreferences(DEFAULT_PREF_FILE_NAME);
            boolean filenameChanged = false, prefixChanged = false;

            if (storeName != null && !storeName.equals(DEFAULT_PREF_FILE_NAME)) {
                prefToWrite = mDatabaseHelper.getPreferences(storeName);
                filenameChanged = true;
            }

            String storedPrefix = null;

            try {
                storedPrefix = prefToWrite.getString(EncryptionManager.getHashed(EncryptionManager.OVERRIDING_KEY_ALIAS_PREFIX_NAME), null);
            } catch (NoSuchAlgorithmException e) {
                throw new MigrationFailedException("Migration to Version: 0.7.0: Failed to hash a key", e);
            } catch (UnsupportedEncodingException e) {
                throw new MigrationFailedException("Migration to Version: 0.7.0: Failed to hash a key", e);
            }

            prefixChanged = storedPrefix == null && keyPrefix != null && !keyPrefix.equals(EncryptionManager.DEFAULT_KEY_ALIAS_PREFIX);

            if ((filenameChanged || prefixChanged) && prefToRead.getAll().size() > 0) {
                try {
                    EncryptionManager readCrypto = new EncryptionManager(mContext, prefToRead, null);
                    EncryptionManager writeCrypto = new EncryptionManager(mContext, prefToWrite, keyPrefix, bitShiftKey, null);

                    Map<String, ?> allData = prefToRead.getAll();

                    for (Map.Entry<String, ?> entry : allData.entrySet()) {
                        String hashedKey = entry.getKey();

                        if (hashedKey.equals(EncryptionManager.getHashed(readCrypto.AES_KEY_ALIAS)) ||
                                hashedKey.equals(EncryptionManager.getHashed(readCrypto.IS_COMPAT_MODE_KEY_ALIAS)) ||
                                hashedKey.equals(EncryptionManager.getHashed(readCrypto.MAC_KEY_ALIAS))) {
                            continue;
                        }

                        if (entry.getValue() == null) {
                            continue;
                        }

                        if (entry.getValue() instanceof Set) { //string set
                            Set<String> values = (Set<String>) entry.getValue();
                            Set<String> eValues = new HashSet<>();

                            for (String value : values) {
                                String dValue = readCrypto.decrypt(value);
                                eValues.add(writeCrypto.encrypt(dValue));
                            }
                            prefToWrite.putStringSet(hashedKey, eValues);
                        } else if (entry.getValue() instanceof String) { //string
                            String dValue = readCrypto.decrypt((String) entry.getValue());
                            prefToWrite.putString(hashedKey, writeCrypto.encrypt(dValue));
                        } else {
                            Logger.e("Found a value that is not String or Set, key: " + hashedKey + ", value: " + entry.getValue());
                        }
                    }

                    if (prefToWrite.flushSync()) {
                        prefToWrite.putInt(VERSION_KEY, DEFAULT_NUM_TEN).flush();
                        cleanupPref(DEFAULT_PREF_FILE_NAME);
                    }
                } catch (InvalidKeyException e) {
                    throw new MigrationFailedException("Migration to Version: 0.7.0: Encryption/Hashing Error", e);
                } catch (UnrecoverableEntryException e) {
                    throw new MigrationFailedException("Migration to Version: 0.7.0: Encryption/Hashing Error", e);
                } catch (KeyStoreException e) {
                    throw new MigrationFailedException("Migration to Version: 0.7.0: Encryption/Hashing Error", e);
                } catch (NoSuchAlgorithmException e) {
                    throw new MigrationFailedException("Migration to Version: 0.7.0: Encryption/Hashing Error", e);
                } catch (NoSuchProviderException e) {
                    throw new MigrationFailedException("Migration to Version: 0.7.0: Encryption/Hashing Error", e);
                } catch (CertificateException e) {
                    throw new MigrationFailedException("Migration to Version: 0.7.0: Encryption/Hashing Error", e);
                } catch (UnsupportedEncodingException e) {
                    throw new MigrationFailedException("Migration to Version: 0.7.0: Encryption/Hashing Error", e);
                } catch (InvalidAlgorithmParameterException e) {
                    throw new MigrationFailedException("Migration to Version: 0.7.0: Encryption/Hashing Error", e);
                } catch (EncryptionManager.InvalidMacException e) {
                    throw new MigrationFailedException("Migration to Version: 0.7.0: Encryption/Hashing Error", e);
                } catch (IllegalBlockSizeException e) {
                    throw new MigrationFailedException("Migration to Version: 0.7.0: Encryption/Hashing Error", e);
                } catch (BadPaddingException e) {
                    throw new MigrationFailedException("Migration to Version: 0.7.0: Encryption/Hashing Error", e);
                } catch (NoSuchPaddingException e) {
                    throw new MigrationFailedException("Migration to Version: 0.7.0: Encryption/Hashing Error", e);
                } catch (IOException e) {
                    throw new MigrationFailedException("Migration to Version: 0.7.0: Encryption/Hashing Error", e);
                }
            }
        }

        void migrate(int fromVersion, int toVersion) throws MigrationFailedException {
            if (fromVersion >= toVersion) {
                return;
            }

            for (int version : VERSIONS_WITH_BREAKING_CHANGES) {
                if (fromVersion < version) {
                    migrate(version);
                    fromVersion = version;
                }
            }

            mPrefs.putInt(VERSION_KEY, toVersion).flush();
        }

        void migrate(int toVersion) throws MigrationFailedException {
            if (toVersion == DEFAULT_NUM_TEN) {
                Logger.d("Migrating to: " + toVersion);
                migrateToV10();
            }
        }

        void cleanupPref(String storeName) {
            Preferences prefs = mDatabaseHelper.getPreferences(storeName);
            if (prefs.getAll().size() > 0) {
                prefs.clear().flushSync();
            }
            mDatabaseHelper.getPreferences(storeName);
        }
    }

    public class MigrationFailedException extends Exception {
        public MigrationFailedException(String message, Throwable cause) {
            super(message, cause);
        }
    }

    @Override
    public Preferences putInt(String s, int i) {
        String val = Integer.toString(i);
        return putString(s, val);
    }

    @Override
    public Preferences putString(String s, String s1) {
        if (isReservedKey(s)) {
            Logger.e("Trying to store value for a reserved key, value: " + s1);
            return this;
        }

        try {
            String hashedKey = EncryptionManager.getHashed(s);
            String evalue = mEncryptionManager.encrypt(s1);
            mPrefs.putString(hashedKey, evalue);
        } catch (Exception e) {
            Logger.e(e);
        }

        return this;
    }

    @Override
    public Preferences putBoolean(String s, boolean b) {
        String val = Boolean.toString(b);
        return putString(s, val);
    }

    @Override
    public Preferences putLong(String s, long l) {
        String val = Long.toString(l);
        return putString(s, val);
    }

    @Override
    public Preferences putFloat(String s, float v) {
        String val = Float.toString(v);
        return putString(s, val);
    }

    @Override
    public Preferences putStringSet(String s, Set<String> set) {
        if (isReservedKey(s)) {
            Logger.e("Trying to store value for a reserved key, value: " + set);
            return this;
        }

        try {
            String hashedKey = EncryptionManager.getHashed(s);
            Set<String> eSet = new HashSet<String>(set.size());

            for (String val : set) {
                eSet.add(mEncryptionManager.encrypt(val));
            }

            mPrefs.putStringSet(hashedKey, eSet);
        } catch (Exception e) {
            Logger.e(e);
        }

        return this;
    }

    @Override
    public Preferences delete(String s) {
        if (isReservedKey(s)) {
            Logger.e("Trying to remove value for a reserved key");
            return this;
        }

        try {
            String hashedKey = EncryptionManager.getHashed(s);
            mPrefs.delete(hashedKey);
        } catch (Exception e) {
            Logger.e(e);
        }

        return this;
    }

    @Override
    public Preferences clear() {
        for (String key : mPrefs.getAll().keySet()) {
            if (key.equals(VERSION_KEY) || isReservedHashedKey(key)) {
                continue;
            }

            mPrefs.delete(key);
        }

        return this;
    }

    @Override
    public void flush() {
        mPrefs.flush();
    }

    @Override
    public boolean flushSync() {
        return mPrefs.flushSync();
    }

    @Override
    public boolean hasKey(String key) {
        try {
            String hashedKey = EncryptionManager.getHashed(key);
            return mPrefs.hasKey(hashedKey);
        } catch (Exception e) {
            Logger.e(e);
        }

        return false;
    }

    @Override
    public void registerObserver(PreferencesObserver preferencesObserver) {

    }

    @Override
    public void unregisterObserver(PreferencesObserver preferencesObserver) {

    }
}
